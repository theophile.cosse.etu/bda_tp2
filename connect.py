import os
import redis_connect
import base64
import uuid
import time
import hashlib

COOKIE_EXPIRE_SEC = 600

user_password_encoded = os.environ['HTTP_AUTHORIZATION'].split()[-1] #Get rid of 'Basic '
user_password_decoded = base64.b64decode(user_password_encoded).decode('utf-8')
user_password_array = user_password_decoded.split(":")
user = user_password_array[0]
password = user_password_array[1]

conn_uuid = redis_connect.connection_redis(1)

if not conn_uuid.exists(user):
    print("HTTP/1.1 400 Bad Request")
    print()
    print("user doesn't exist")
    exit()


user_uuid = conn_uuid[user]

conn_user_data = redis_connect.connection_redis(2)
user_data = conn_user_data.hgetall(user_uuid)

salt = user_data[b"salt"]
key = user_data[b"key"]
key_to_check = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 100000)

if key != key_to_check:
    print("HTTP/1.1 400 Bad Request")
    print()
    print("Wrong password")
    exit()   

user_data[b"last_connection"] = time.time()
conn_user_data.hmset(user_uuid, user_data)

conn_user_temporary_uuid = redis_connect.connection_redis(5)
new_user_uuid = uuid.uuid4()
conn_user_temporary_uuid.set(new_user_uuid.bytes, user_uuid, ex=COOKIE_EXPIRE_SEC)

print("HTTP/1.1 200 OK")
#print("Set-Cookie: sessionId="+str(new_user_uuid)+"; Max-Age="+str(COOKIE_EXPIRE_SEC))
print("Set-Cookie: sessionId="+str(new_user_uuid))
print()
print("TOUT EST OK (a priori)")
print()



