import os
import redis_connect
import time
import uuid
import json 
import datetime

conn_user_temporary_uuid = redis_connect.connection_redis(5)
if not 'HTTP_COOKIE' in os.environ:
    print("HTTP/1.1 200 OK")
    print()
    print("PAS DE COOKIE")
    exit()   


cookie = os.environ['HTTP_COOKIE'].split('=')[-1]
session_uuid = uuid.UUID("{"+cookie+"}") # "{" "}" a cause de la syntax bizzare de uuid.UUID



if not conn_user_temporary_uuid.exists(session_uuid.bytes):
    print("HTTP/1.1 400 Bad Request")
    print("content-type: application/json")
    print("COOKIE EXPIRED")
    exit()


user_uuid = conn_user_temporary_uuid[session_uuid.bytes]

conn_user_data = redis_connect.connection_redis(2)
user_data = conn_user_data.hgetall(user_uuid)
#On dirait que json.dumps n'aime pas les champs contenant du binaire

user_data_for_json = {
    "name":user_data[b"name"].decode('utf-8'),
    "user_creation":datetime.datetime.fromtimestamp(float(user_data[b"user_creation"].decode('utf-8'))).strftime('%c'),
    "last_connection":datetime.datetime.fromtimestamp(float(user_data[b"last_connection"].decode('utf-8'))).strftime('%c'),
}

json_data = json.dumps(user_data_for_json)

print("HTTP/1.1 200 OK")
print("Content-Type: application/json")
print()
print(json_data)
