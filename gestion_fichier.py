import os
import redis_connect
import time
import uuid
import json 
import datetime
import sys
import shutil



#### connection
conn_user_temporary_uuid = redis_connect.connection_redis(5)
if not 'HTTP_COOKIE' in os.environ:
    print("HTTP/1.1 200 OK")
    print()
    print("PAS DE COOKIE")
    exit()   
cookie = os.environ['HTTP_COOKIE'].split('=')[-1]
session_uuid = uuid.UUID("{"+cookie+"}") # "{" "}" a cause de la syntax bizzare de uuid.UUID
if not conn_user_temporary_uuid.exists(session_uuid.bytes):
    print("HTTP/1.1 400 Bad Request")
    print("content-type: application/json")
    print("COOKIE EXPIRED")
    exit()
user_uuid = conn_user_temporary_uuid[session_uuid.bytes]
####


#user_data = conn_user_data.hgetall(user_uuid)

"""
in_file = open("in-file", "rb") # opening for [r]eading as [b]inary
data = in_file.read() # if you only wanted to read 512 bytes, do .read(512)
in_file.close()

out_file = open("out-file", "wb") # open for [w]riting as [b]inary
out_file.write(data)
out_file.close()
"""



#with os.fdopen(sys.stdin.fileno(), 'rb') as input_file, open("/home/ubuntu/test.txt", 'wb') as output_file:
#    shutil.copyfileobj(input_file, output_file)



method = os.environ['REQUEST_METHOD'].split('=')[-1]

filename = os.environ['REQUEST_URI'].split('=')[-1].split('/')[-1]
user_uuid_fichier = redis_connect.connection_redis(4)

if method == "POST" or method == "PUT" or method == "PATCH":
    data_file = sys.stdin.buffer.read()
    file_uuid = uuid.uuid4().bytes
    user_uuid_fichier.hset(user_uuid, filename, file_uuid)
    #Store file
    liste_fichier = redis_connect.connection_redis(3)
    liste_fichier.lpush(file_uuid, data_file)

    print("HTTP/1.1 200 OK")
    print()
    print("UPLOAD DE FICHIER")
    print("FICHIER " + filename + " UPLOADED")

if method == "GET":

    if filename == "":
        file_list_data = user_uuid_fichier.hgetall(user_uuid)
        json_result = {"data" : []}
        for key in file_list_data.keys():
            key_utf = key.decode('utf-8')
            json_result["data"].append(key_utf)

        json_data = json.dumps(json_result)

        print("HTTP/1.1 200 OK")
        print("Content-Type: application/json")
        print()
        print(json_data)
        
    else:
        file_uuid = user_uuid_fichier.hget(user_uuid,str.encode(filename))
        liste_fichier = redis_connect.connection_redis(3)
        file_data = liste_fichier.lindex(file_uuid,int(liste_fichier.llen(file_uuid))-1)
        if (file_data != None):
            print("Content-Disposition: attachment; filename=" + filename)
            print("Content-Type: application/octet-stream")
            
            sys.stdout.flush()
            sys.stdout.buffer.write(file_data)
        else:
            print("HTTP/1.1 400 Bad Request")
            print()
            print("Nothing to download")
            exit()
if method == "DELETE":
    file_uuid = user_uuid_fichier.hget(user_uuid,str.encode(filename))
    liste_fichier = redis_connect.connection_redis(3)
    liste_fichier.lpop(file_uuid)
    print("HTTP/1.1 200 OK")
    print()
    print("FICHIER " + filename + " SUPPRIMER")











