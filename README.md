redis_ip=172.28.101.61
middleware_ip=172.28.101.43
pass_redis=1234 

SELECT permet de changer de base de donnée (mais en fait tout est stocké dans le meme fichier, c'est juste une couche logique)

$middleware_ip/env.cgi retourne la liste des variable d'environnement -> information sur la requete HTTP

curl $middleware_ip/env le serveur middleware execute le script python env.py et renvoie stout dans le corp de la requette http


# Utilisation du middleware

create account : curl --user user:password $middleware_ip/create

Pour faciliter les test les cookie expire au bout de 10 min

curl -c cookies_file --user user:password $middleware_ip/connect

curl -b cookies_file $middleware_ip

curl -b cookies_file -F 'data=@text.txt' $middleware_ip/files/test.md  

curl -b cookies_file $middleware_ip/files/   

curl -b cookies_file $middleware_ip/files/test.md --output res.md

curl -b cookies_file -X "DELETE" $middleware_ip/files/test.md  

# Remarque

J'ai surement choisi le pire format de date, j'ai utilisé les [date unix](https://docs.python.org/3/library/time.html#time.time) sous forme de chaine de charactére.
Ainsi pour lire la date j'ai besoin de : 
- Recuperer l'information stocké en binaire par redis
- Décoder en utf-8
- Parser en float
- Creer un objet date a partir de ce float
- formatter la date

Sois : 
```python
datetime.datetime.fromtimestamp(float(user_data[b"last_connection"].decode('utf-8'))).strftime('%c')
```

Je n'ai pas reussi le mapping "/files/\\\*", j'ai dû utiliser la regexp "/files/.*".
Pas besoin d'escape \*. l'asterisque spécifie qu'un caractère peut être répété, il faut donc spécifier ce caractère, ici . match n'importe quel caractère.