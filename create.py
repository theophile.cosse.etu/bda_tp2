import os
import redis_connect
import base64
import uuid
import time
import hashlib

user_password_encoded = os.environ['HTTP_AUTHORIZATION'].split()[-1] #Get rid of 'Basic '
user_password_decoded = base64.b64decode(user_password_encoded).decode('utf-8')
user_password_array = user_password_decoded.split(":")
user = user_password_array[0]
password = user_password_array[1]

conn_uuid = redis_connect.connection_redis(1)

if conn_uuid.exists(user):
    print("HTTP/1.1 400 Bad Request")
    print()
    print("user already exist")
    exit()

user_uuid = uuid.uuid4().bytes

conn_uuid[user] = user_uuid

conn_user_data = redis_connect.connection_redis(2)

salt = os.urandom(32)
key = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt, 100000)
user_data = {
    "name":user,
    "user_creation":time.time(),
    "last_connection":time.time(),
    "key":key,
    "salt":salt
}

conn_user_data.hmset(user_uuid, user_data)

print("HTTP/1.1 200 OK")
print()
print("user registerd")
